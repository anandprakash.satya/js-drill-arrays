 // Flattens a nested array (the nesting can be to any depth).
    // Hint: You can solve this using recursion.
    // Example: flatten([1, [2], [3, [[4]]]]); => [1, 2, 3, 4];

function flatten(array){
    let arr=[]
    for(let i of array){
        if(Array.isArray(i)){
            arr = arr.concat(flatten(i))
        }else{
            arr.push(i)
        }
    }
    return arr
}
module.exports = flatten