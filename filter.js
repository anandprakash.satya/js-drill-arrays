let items = [1,2,3,4,5,5]
 // Do NOT use .filter, to complete this function.
    // Similar to `find` but you will return an array of all elements that passed the truth test
    // Return an empty array if no elements pass the truth test

function filter(elements,cb){
    let arr =[]
    for(let i of elements){
        if(cb(i)){
            arr.push(i)
        }

    }
    return arr
}
module.exports = filter