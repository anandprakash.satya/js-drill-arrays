let items = [1,2,3,4,5,5]

 // Do NOT use forEach to complete this function.
    // Iterates over a list of elements, yielding each in turn to the `cb` function.
    // This only needs to work with arrays.
    // You should also pass the index into `cb` as the second argument

function each(elements,cb){
    
    for(let i=0;i<elements.length;i++){

        cb(elements[i])
    }
}
function cb(items){
    console.log(items+1)
}

module.exports = each